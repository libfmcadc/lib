/*
 * Initializing and cleaning up the adc library
 *
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2 as published by the Free Software Foundation or, at your
 * option, any later version.
 */
#include <string.h>
#include "adc-lib.h"
#include "adc-lib-int.h"

/* * * * * * * * * * * * * * * * Utilities * * * * * * * * * * * * * * * * */
/*
 * adc_strerror
 * @dev: device for which you want to know the meaning of the error
 * @errnum: error number
 */

static struct adc_errors {
	int num;
	char *str;
} adc_errors[] = {
	{ ADC_ENOP,		"Operation not supported"},
	{ ADC_ENOCAP,	"Capabilities not supported"},
	{ ADC_ENOCFG,	"Configuration type not supported"},
	{ ADC_ENOGET,	"Cannot get capabilities information"},
	{ ADC_ENOSET,	"Cannot set capabilities information"},
	{ ADC_ENOCHAN,	"Invalid channel"},
	{ ADC_ENOMASK,	"Missing configuration mask"},
	{ ADC_EDISABLED,	"Trigger is disabled: I/O aborted"},
	{ 0, }
};

char *adc_strerror(int errnum)
{
	struct adc_errors *p;

	if (errnum < __ADC_ERRNO_START)
		return strerror(errnum);
	for (p = adc_errors; p->num; p++)
		if (p->num == errnum)
			return p->str;
	return "Unknown error code";
}

/*
 * adc_get_driver_type
 * @dev: device which want to know the driver type
 */
char *adc_get_driver_type(struct adc_dev *dev)
{
	struct adc_gid *b = (void *)dev;

	return b->board->driver_type;
}
