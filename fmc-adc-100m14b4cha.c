/*
 * The ADC library for the specific card
 *
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2 as published by the Free Software Foundation or, at your
 * option, any later version.
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <linux/zio-user.h>
#include <fmc-adc-100m14b4cha.h>

#include "adc-lib.h"
#include "adc-lib-int.h"

#define ZIO_SYS_PATH "/sys/bus/zio/devices"

#define ADC_NCHAN 4


struct adc_dev *adc_100m14b4cha_open(const struct adc_board_type *b,
				   unsigned int dev_id,
				   unsigned long totalsamples,
				   unsigned int nbuffer,
				   unsigned long flags)
{
	struct adc_dev *dev = adc_zio_open(b, dev_id, totalsamples, nbuffer, flags);
	
	/*
	 * We need to save the page size and samplesize.
	 * Samplesize includes the nchan in the count.
	 */
	
	struct __adc_dev_zio *fa = to_dev_zio(dev);
	fa->samplesize = 8; /* FIXME: should read sysfs instead -- where? */
	dev = (void *) &fa->gid;
	

	return dev;
}


int adc_100m14b4cha_acq_start(struct adc_dev *dev,
			 unsigned int flags, struct timeval *timeout)
{
	struct __adc_dev_zio *fa = to_dev_zio(dev);
	uint32_t cmd = 1; /* hw command for "start" */
	int err;
	
	err = adc_zio_sysfs_set(fa, "cset0/fsm-command", &cmd);
	if (err)
		return err;


	return adc_zio_acq_start(dev, flags, timeout);
}

int adc_100m14b4cha_acq_stop(struct adc_dev *dev,	unsigned int flags)
{
	struct __adc_dev_zio *fa = to_dev_zio(dev);
	uint32_t cmd = 2; /* hw command for "stop" */

	return adc_zio_sysfs_set(fa, "cset0/fsm-command", &cmd);
}



