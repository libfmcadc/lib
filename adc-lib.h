/*
 * Copyright CERN 2013
 * Author: Federico Vaga <federico.vaga@gmail.com>
 */

#ifndef ADC_LIB_H_
#define ADC_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>

/* Error codes start from 1024 to void conflicting with libc codes */
#define __ADC_ERRNO_START 1024
#define ADC_ENOP		1024
#define ADC_ENOCAP		1025
#define ADC_ENOCFG		1026
#define ADC_ENOGET		1027
#define ADC_ENOSET		1028
#define ADC_ENOCHAN		1029
#define ADC_ENOMASK		1030
#define ADC_EDISABLED		1031

struct adc_dev;

enum adc_supported_boards {
	FMCADC_100MS_4CH_14BIT,
	ADC_ZIOFAKE,
	ADC_GENERICFAKE,
	__ADC_SUPPORTED_BOARDS_LAST_INDEX,
};

/* The buffer hosts data and metadata, plus informative fields */
struct adc_buffer {
	void *data;
	void *metadata;
	int samplesize;
	int nsamples;
	struct adc_dev *dev;
	void *mapaddr;
	unsigned long maplen;
	unsigned long flags; /* internal to the library */
};

/* This is exactly the zio_timestamp, there is no depency on zio here */
struct adc_timestamp {
	uint64_t secs;
	uint64_t ticks;
	uint64_t bins;
};

/* The following enum can be use to se the mask of valid configurations */
enum adc_configuration_trigger {
	ADC_CONF_TRG_SOURCE = 0,
	ADC_CONF_TRG_SOURCE_CHAN,
	ADC_CONF_TRG_THRESHOLD,
	ADC_CONF_TRG_POLARITY,
	ADC_CONF_TRG_DELAY,
	ADC_CONF_TRG_THRESHOLD_FILTER,
	__ADC_CONF_TRG_ATTRIBUTE_LAST_INDEX,
};
enum adc_configuration_acquisition {
	ADC_CONF_ACQ_N_SHOTS = 0,
	ADC_CONF_ACQ_POST_SAMP,
	ADC_CONF_ACQ_PRE_SAMP,
	ADC_CONF_ACQ_DECIMATION,
	ADC_CONF_ACQ_FREQ_HZ,
	ADC_CONF_ACQ_N_BITS,
	__ADC_CONF_ACQ_ATTRIBUTE_LAST_INDEX,
};
enum adc_configuration_channel {
	ADC_CONF_CHN_RANGE = 0,
	ADC_CONF_CHN_TERMINATION,
	ADC_CONF_CHN_OFFSET,
	ADC_CONF_CHN_SATURATION,
	__ADC_CONF_CHN_ATTRIBUTE_LAST_INDEX,
};
enum adc_board_status {
	ADC_CONF_BRD_STATUS = 0,
	ADC_CONF_BRD_MAX_FREQ_HZ,
	ADC_CONF_BRD_MIN_FREQ_HZ,
	ADC_CONF_BRD_STATE_MACHINE_STATUS,
	ADC_CONF_BRD_N_CHAN,
	ADC_CONF_UTC_TIMING_BASE_S,
	ADC_CONF_UTC_TIMING_BASE_T,
	ADC_CONF_UTC_TIMING_BASE_B,
	__ADC_CONF_BRD_ATTRIBUTE_LAST_INDEX,
};
enum adc_configuration_type {
	ADC_CONF_TYPE_TRG = 0,	/* Trigger */
	ADC_CONF_TYPE_ACQ,		/* Acquisition */
	ADC_CONF_TYPE_CHN,		/* Channel */
	ADC_CONT_TYPE_BRD,		/* Board */
	__ADC_CONF_TYPE_LAST_INDEX,
};


#define __ADC_CONF_LEN 64 /* number of allocated items in each structure */
struct adc_conf {
	enum adc_configuration_type type;
	uint32_t dev_type;
	uint32_t route_to;
	uint32_t flags; /* how to identify invalid? */
	uint64_t mask;
	uint32_t value[__ADC_CONF_LEN];
};


static inline void adc_set_conf_mask(struct adc_conf *conf,
				        unsigned int conf_index)
{
	conf->mask |= (1LL << conf_index);
}

/* assign a configuration item, and its mask */
static inline void adc_set_conf(struct adc_conf *conf,
				   unsigned int conf_index, uint32_t val)
{
	conf->value[conf_index] = val;
	adc_set_conf_mask(conf, conf_index);
}

/* retieve a configuration item */
static inline int adc_get_conf(struct adc_conf *conf,
				  unsigned int conf_index,
				  uint32_t *val)
{
	if (conf->mask & (1LL << conf_index)) {
		*val = conf->value[conf_index];
		return 0;
	} else {
		return -1;
	}
}

/* Flags used in open/acq/config -- note: low-bits are used by lib-int.h */
#define ADC_F_USERMASK	0xffff0000	//found as FMCSDC_F_USERMASK instead of FMCADC_F_USERMASK was it a mistake?
#define ADC_F_FLUSH	0x00010000
#define ADC_F_VERBOSE	0x00020000

/*
 * Actual functions follow
 */
extern int adc_init(void);
extern void adc_exit(void);
extern char *adc_strerror(int errnum);

extern struct adc_dev *adc_open(char *name, unsigned int dev_id,
				      unsigned long totalsamples,
				      unsigned int nbuffer,
				      unsigned long flags);
extern struct adc_dev *adc_open_by_lun(char *name, int lun,
					     unsigned long totalsamples,
					     unsigned int nbuffer,
					     unsigned long flags);
extern int adc_close(struct adc_dev *dev);

extern int adc_acq_start(struct adc_dev *dev, unsigned int flags,
			    struct timeval *timeout);
extern int adc_acq_poll(struct adc_dev *dev, unsigned int flags,
			    struct timeval *timeout);
extern int adc_acq_stop(struct adc_dev *dev, unsigned int flags);

extern int adc_reset_conf(struct adc_dev *dev, unsigned int flags,
			       struct adc_conf *conf);
extern int adc_apply_config(struct adc_dev *dev, unsigned int flags,
			       struct adc_conf *conf);
extern int adc_retrieve_config(struct adc_dev *dev,
				 struct adc_conf *conf);
extern int adc_get_param(struct adc_dev *dev, char *name,
			    char *sptr, int *iptr);
extern int adc_set_param(struct adc_dev *dev, char *name,
			    char *sptr, int *iptr);

extern struct adc_buffer *adc_request_buffer(struct adc_dev *dev,
						   int nsamples,
						   void *(*alloc_fn)(size_t),
						   unsigned int flags);
extern int adc_fill_buffer(struct adc_dev *dev,
			      struct adc_buffer *buf,
			      unsigned int flags,
			      struct timeval *timeout);
extern struct adc_timestamp *adc_tstamp_buffer(struct adc_buffer *buf,
						     struct adc_timestamp *);
extern int adc_release_buffer(struct adc_dev *dev,
				 struct adc_buffer *buf,
				 void (*free_fn)(void *));

extern char *adc_get_driver_type(struct adc_dev *dev);

#ifdef __cplusplus
}
#endif

#endif /* ADC_LIB_H_ */
