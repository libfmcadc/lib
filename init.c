/*
 * Copyright CERN 2013, GNU GPL 2 or later.
 * Author: Alessandro Rubini
 */

#include "adc-lib.h"

/* We currently do nothing in init/exit. We might check /proc/meminfo... */
int adc_init(void)
{
	return 0;
}

void adc_exit(void)
{
	return;
}
