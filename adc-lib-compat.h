/*It depends on the header fmcadc-lib.h. Include this file after fmcadc-lib.h*/

#define fmcadc_dev 		adc_dev
#define fmcadc_buffer 		adc_buffer
#define fmcadc_timestamp 	adc_timestamp
#define fmcadc_conf		adc_conf

#define __fmcadc_dev_zio	__adc_dev_zio
#define fmcadc_board_type	adc_board_type
#define fmcadc_operations	adc_operations
#define fmcadc_gid		adc_gid
#define __fmcadc_genfake_conf	__adc_genfake_conf
#define __fmcadc_dev_genfake	__adc_dev_genfake

#define __FMCADC_ERRNO_START	__ADC_ERRNO_START
#define FMCADC_ENOP		ADC_ENOP
#define FMCADC_ENOCAP		ADC_ENOCAP
#define FMCADC_ENOCFG		ADC_ENOCFG
#define FMCADC_ENOGET		ADC_ENOGET
#define FCMADC_ENOSET		ADC_ENOSET
#define FMCADC_ENOCHAN		ADC_ENOCHAN
#define FMCADC_ENOMASK		ADC_ENOMASK
#define FMCADC_EDISABLED	ADC_EDISABLED


#define	FMCADC_CONF_TRG_SOURCE			ADC_CONF_TRG_SOURCE
#define	FMCADC_CONF_TRG_SOURCE_CHAN		ADC_CONF_TRG_SOURCE_CHAN
#define	FMCADC_CONF_TRG_THRESHOLD		ADC_CONF_TRG_THRESHOLD
#define	FMCADC_CONF_TRG_POLARITY		ADC_CONF_TRG_POLARITY
#define	FMCADC_CONF_TRG_DELAY			ADC_CONF_TRG_DELAY
#define	FMCADC_CONF_TRG_THRESHOLD_FILTER	ADC_CONF_TRG_THRESHOLD_FILTER
#define	__FMCADC_CONF_TRG_ATTRIBUTE_LAST_INDEX	__ADC_CONF_TRG_ATTRIBUTE_LAST_INDEX

#define	FMCADC_CONF_ACQ_N_SHOTS			ADC_CONF_ACQ_N_SHOTS
#define	FMCADC_CONF_ACQ_POST_SAMP		ADC_CONF_ACQ_POST_SAMP
#define	FMCADC_CONF_ACQ_PRE_SAMP		ADC_CONF_ACQ_PRE_SAMP
#define	FMCADC_CONF_ACQ_DECIMATION		ADC_CONF_ACQ_DECIMATION
#define	FMCADC_CONF_ACQ_FREQ_HZ			ADC_CONF_ACQ_FREQ_HZ
#define	FMCADC_CONF_ACQ_N_BITS			ADC_CONF_ACQ_N_BITS
#define	__FMCADC_CONF_ACQ_ATTRIBUTE_LAST_INDEX	__ADC_CONF_ACQ_ATTRIBUTE_LAST_INDEX

#define	FMCADC_CONF_CHN_RANGE			ADC_CONF_CHN_RANGE
#define	FMCADC_CONF_CHN_TERMINATION		ADC_CONF_CHN_TERMINATION
#define	FMCADC_CONF_CHN_OFFSET			ADC_CONF_CHN_OFFSET
#define	FMCADC_CONF_CHN_SATURATION		ADC_CONF_CHN_SATURATION
#define	__FMCADC_CONF_CHN_ATTRIBUTE_LAST_INDEX	__ADC_CONF_CHN_ATTRIBUTE_LAST_INDEX

#define	FMCADC_CONF_BRD_STATUS			ADC_CONF_BRD_STATUS
#define	FMCADC_CONF_BRD_MAX_FREQ_HZ		ADC_CONF_BRD_MAX_FREQ_HZ
#define	FMCADC_CONF_BRD_MIN_FREQ_HZ		ADC_CONF_BRD_MIN_FREQ_HZ
#define	FMCADC_CONF_BRD_STATE_MACHINE_STATUS	ADC_CONF_BRD_STATE_MACHINE_STATUS
#define	FMCADC_CONF_BRD_N_CHAN			ADC_CONF_BRD_N_CHAN
#define	FMCADC_CONF_UTC_TIMING_BASE_S		ADC_CONF_UTC_TIMING_BASE_S
#define	FMCADC_CONF_UTC_TIMING_BASE_T		ADC_CONF_UTC_TIMING_BASE_T
#define	FMCADC_CONF_UTC_TIMING_BASE_B		ADC_CONF_UTC_TIMING_BASE_B
#define	__FMCADC_CONF_BRD_ATTRIBUTE_LAST_INDEX	__ADC_CONF_BRD_ATTRIBUTE_LAST_INDEX

#define	FMCADC_CONF_TYPE_TRG			ADC_CONF_TYPE_TRG
#define	FMCADC_CONF_TYPE_ACQ			ADC_CONF_TYPE_ACQ
#define	FMCADC_CONF_TYPE_CHN			ADC_CONF_TYPE_CHN
#define	FMCADC_CONT_TYPE_BRD			ADC_CONT_TYPE_BRD
#define	__FMCADC_CONF_TYPE_LAST_INDEX		__ADC_CONF_TYPE_LAST_INDEX

#define __FMCADC_CONF_LEN	__ADC_CONF_LEN

#define FMCADC_F_USERMASK	ADC_F_USERMASK	//found as FMCSDC_F_USERMASK was it a mistake?
#define FMCADC_F_FLUSH		ADC_F_FLUSH
#define FMCADC_F_VERBOSE	ADC_F_VERBOSE

static inline int fmcadc_init(void)
{
	return adc_init();
}

static inline void fmcadc_exit(void)
{
	return adc_exit();
}

static inline char *fmcadc_strerror(int errnum)
{
	return adc_strerror(errnum);
}

static inline char *fmcadc_get_driver_type(struct fmcadc_dev *dev)
{
	return adc_get_driver_type(dev);
}


static inline struct fmcadc_dev *fmcadc_open(char *name, unsigned int dev_id,
				      unsigned long totalsamples,
				      unsigned int nbuffer,
				      unsigned long flags)
{
	return adc_open(name, dev_id, totalsamples, nbuffer, flags);
}

static inline struct fmcadc_dev *fmcadc_open_by_lun(char *name, int lun,
					     unsigned long totalsamples,
					     unsigned int nbuffer,
					     unsigned long flags)
{				     
	return adc_open_by_lun(name, lun, totalsamples, nbuffer, flags);
}

static inline int fmcadc_close(struct fmcadc_dev *dev)
{
	return adc_close(dev);
}


static inline int fmcadc_acq_start(struct fmcadc_dev *dev,
			     unsigned int flags,
			     struct timeval *timeout)
{
	return adc_acq_start(dev, flags, timeout);
}

static inline int fmcadc_acq_poll(struct fmcadc_dev *dev, unsigned int flags,
		    struct timeval *timeout)
{
	return adc_acq_poll(dev, flags, timeout);
}

static inline int fmcadc_acq_stop(struct fmcadc_dev *dev, unsigned int flags)
{
	return adc_acq_stop(dev, flags);
}


static inline int fmcadc_reset_conf(struct fmcadc_dev *dev, unsigned int flags,
			       struct adc_conf *conf)
{
	return adc_reset_conf(dev, flags, conf);
}

static inline int fmcadc_apply_config(struct fmcadc_dev *dev, unsigned int flags,
			       struct adc_conf *conf)
{
	return adc_apply_config(dev, flags, conf);
}

static inline int fmcadc_retrieve_config(struct fmcadc_dev *dev,
					struct adc_conf *conf)
{
	return adc_retrieve_config(dev, conf);
}


static inline int fmcadc_set_param(struct fmcadc_dev *dev, char *name,
			 				char *sptr, int *iptr)
{
	return adc_set_param(dev, name, sptr, iptr);
}

static inline int fmcadc_get_param(struct fmcadc_dev *dev, char *name,
			 		char *sptr, int *iptr)
{
	return adc_get_param(dev, name, sptr, iptr);
}


static inline struct adc_buffer *fmcadc_request_buffer(struct fmcadc_dev *dev,
						   int nsamples,
						   void *(*alloc_fn)(size_t),
						   unsigned int flags)
{
	return adc_request_buffer(dev, nsamples, alloc_fn, flags);
}

static inline int fmcadc_fill_buffer(struct fmcadc_dev *dev,
			      struct adc_buffer *buf,
			      unsigned int flags,
			      struct timeval *timeout)
{			      
	return adc_fill_buffer(dev, buf, flags, timeout);
}

static inline struct fmcadc_timestamp *fmcadc_tstamp_buffer(struct adc_buffer *buf,
						     struct fmcadc_timestamp *ts)
{
	return adc_tstamp_buffer(buf, ts);
}

static inline int fmcadc_release_buffer(struct fmcadc_dev *dev,
				 struct adc_buffer *buf,
				 void (*free_fn)(void *))
{				 
	return adc_release_buffer(dev, buf, free_fn);
}



static inline void fmcadc_set_conf_mask(struct adc_conf *conf,
				        unsigned int conf_index)
{
	return adc_set_conf_mask(conf, conf_index);
}

static inline void fmcadc_set_conf(struct adc_conf *conf,
				   unsigned int conf_index, uint32_t val)
{
	return adc_set_conf(conf, conf_index, val);
}

static inline int fmcadc_get_conf(struct adc_conf *conf,
				  unsigned int conf_index,
				  uint32_t *val)
{
	return adc_get_conf(conf, conf_index, val);
}



