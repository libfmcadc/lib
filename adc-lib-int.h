/*
 * Copyright CERN 2013
 * Author: Federico Vaga <federico.vaga@gmail.com>
 */
#ifndef ADC_LIB_INT_H_
#define ADC_LIB_INT_H_

/*
 * offsetof and container_of come from kernel.h header file
 */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = ((void *)ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})
#define to_dev_zio(dev) (container_of(dev, struct __adc_dev_zio, gid))

/* ->open takes different args than open(), so fa a fun to use tpyeof */
struct adc_board_type;
struct adc_dev *adc_internal_open(const struct adc_board_type *b,
					unsigned int dev_id,
					unsigned long totalsamples,
					unsigned int nbuffer,
					unsigned long flags);

/*
 * The operations structure is the device-specific backend of the library
 */
struct adc_operations {
	typeof(adc_internal_open)	*open;
	typeof(adc_close)		*close;

	typeof(adc_acq_start)		*acq_start;
	typeof(adc_acq_poll)		*acq_poll;
	typeof(adc_acq_stop)		*acq_stop;

	typeof(adc_apply_config)	*apply_config;
	typeof(adc_retrieve_config)	*retrieve_config;
	typeof(adc_get_param)		*get_param;
	typeof(adc_set_param)		*set_param;

	typeof(adc_request_buffer)	*request_buffer;
	typeof(adc_fill_buffer)		*fill_buffer;
	typeof(adc_tstamp_buffer)	*tstamp_buffer;
	typeof(adc_release_buffer)	*release_buffer;
};
/*
 * This structure describes the board supported by the library
 * @name name of the board type, for example "fmc-adc-100MS"
 * @devname name of the device in Linux
 * @driver_type: the kind of driver that hanlde this kind of board (e.g. ZIO)
 * @capabilities bitmask of device capabilities for trigger, channel
 *               acquisition
 * @adc_op pointer to a set of operations
 */
struct adc_board_type {
	char			*name;
	char			*devname;
	char			*driver_type;
	uint32_t		capabilities[__ADC_CONF_TYPE_LAST_INDEX];
	struct adc_operations *adc_op;
};

/*
 * Generic Instance Descriptor
 */
struct adc_gid {
	const struct adc_board_type *board;
};

/* Definition of board types */
extern struct adc_board_type fmcadc_100ms_4ch_14bit;
extern struct adc_board_type adc_ziofake;
extern struct adc_board_type adc_genericfake;

/* Internal structure (ZIO specific, for ZIO drivers only) */
struct __adc_dev_zio {
	unsigned int cset;
	int fdc;
	int fdd;
	uint32_t dev_id;
	unsigned long flags;
	char *devbase;
	char *sysbase;
	unsigned long samplesize;
	unsigned long pagesize;
	/* Mandatory field */
	struct adc_gid gid;
};

struct __adc_genfake_conf {
	int nshots;
	int presamples;
	int postsamples;
};

/* Internal structure for generic (not ZIO) fake board type*/
struct __adc_dev_genfake {
	int acq;
	unsigned int conf_index;
	unsigned int conf_type;
	unsigned long samplesize;
	struct __adc_genfake_conf *cf;
	struct adc_gid gid;
};

/* Note: bit 16 and up are passed by users, see fmcadc-lib.h */
#define ADC_FLAG_VERBOSE 0x00000001
#define ADC_FLAG_MALLOC  0x00000002 /* allocate data */
#define ADC_FLAG_MMAP    0x00000004 /* mmap data */

/* The board-specific functions are defined in fmc-adc-100m14b4cha.c */

/*fmc-adc-100m14b4cha*/

struct adc_dev *adc_100m14b4cha_open(const struct adc_board_type *b,
				   unsigned int dev_id,
				   unsigned long totalsamples,
				   unsigned int nbuffer,
				   unsigned long flags);

int adc_100m14b4cha_acq_start(struct adc_dev *dev,
			 	unsigned int flags, 
			 	struct timeval *timeout);

int adc_100m14b4cha_acq_stop(struct adc_dev *dev,
				unsigned int flags);


/*adc-ziofake*/

struct adc_dev *adc_ziofake_open(const struct adc_board_type *b,
				   unsigned int dev_id,
				   unsigned long totalsamples,
				   unsigned int nbuffer,
				   unsigned long flags);


/*ZIO boards*/				   

struct adc_dev *adc_zio_open(const struct adc_board_type *b,
				   unsigned int dev_id,
				   unsigned long totalsamples,
				   unsigned int nbuffer,
				   unsigned long flags);

int adc_zio_close(struct adc_dev *dev);

int adc_zio_acq_start(struct adc_dev *dev,
			unsigned int flags, 
			struct timeval *timeout);
			 
int adc_zio_acq_poll(struct adc_dev *dev, 
			unsigned int flags,
			struct timeval *timeout);

int adc_zio_acq_stop(struct adc_dev *dev, unsigned int flags);

struct adc_buffer *adc_zio_request_buffer(struct adc_dev *dev,
						int nsamples,
						void *(*alloc)(size_t),
						unsigned int flags);

int adc_zio_fill_buffer(struct adc_dev *dev,
			   struct adc_buffer *buf,
			   unsigned int flags,
			   struct timeval *timeout);

struct adc_timestamp *adc_zio_tstamp_buffer(struct adc_buffer *buf,
						  struct adc_timestamp *);

int adc_zio_release_buffer(struct adc_dev *dev,
			      struct adc_buffer *buf,
			      void (*free_fn)(void *));

		/* The following functions are in config-zio.c */
int adc_zio_apply_config(struct adc_dev *dev, unsigned int flags,
			    struct adc_conf *conf);
			    
int adc_zio_retrieve_config(struct adc_dev *dev,
			       struct adc_conf *conf);

int adc_zio_set_param(struct adc_dev *dev, char *name,
			 char *sptr, int *iptr);

int adc_zio_get_param(struct adc_dev *dev, char *name,
			 char *sptr, int *iptr);

int adc_zio_sysfs_set(struct __adc_dev_zio *fa, char *name,
		     uint32_t *value);


/*adc-genericfake*/

struct adc_dev *adc_genfake_open(const struct adc_board_type *b,
					unsigned int dev_id,
					unsigned long totalsamples,
					unsigned int nbuffer,
					unsigned long flags);

int adc_genfake_close(struct adc_dev *dev);

int adc_genfake_acq_start(struct adc_dev *dev,
			 	unsigned int flags, 
			 	struct timeval *timeout);

int adc_genfake_acq_poll(struct adc_dev *dev, 
				unsigned int flags,
		    		struct timeval *timeout);

int adc_genfake_acq_stop(struct adc_dev *dev, unsigned int flags);

struct adc_buffer *adc_genfake_request_buffer(struct adc_dev *dev,
						   int nsamples,
						   void *(*alloc_fn)(size_t),
						   unsigned int flags);

int adc_genfake_fill_buffer(struct adc_dev *dev,
			      struct adc_buffer *buf,
			      unsigned int flags,
			      struct timeval *timeout);

struct adc_timestamp *adc_genfake_tstamp_buffer(struct adc_buffer *buf,
						struct adc_timestamp *ts);

int adc_genfake_release_buffer(struct adc_dev *dev,
				 struct adc_buffer *buf,
				 void (*free_fn)(void *));

int adc_genfake_apply_config(struct adc_dev *dev, unsigned int flags, 
				struct adc_conf *conf);

int adc_genfake_retrieve_config(struct adc_dev *dev,
			       struct adc_conf *conf);

int adc_genfake_set_param(struct adc_dev *dev, char *name,
				char *sptr, int *iptr);

int adc_genfake_get_param(struct adc_dev *dev, char *name, 
				char *sptr, int *iptr);

#endif /* ADC_LIB_INT_H_ */
