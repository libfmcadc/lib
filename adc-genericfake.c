#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <poll.h>
#include <sys/stat.h>
#include <time.h>

#include "adc-lib.h"
#include "adc-lib-int.h"

struct adc_dev *adc_genfake_open(const struct adc_board_type *b,
					unsigned int dev_id,
					unsigned long totalsamples,
					unsigned int nbuffer,
					unsigned long flags)
{
	//fprintf(stderr, "%s: line %d\n", __FILE__, __LINE__);
	struct __adc_dev_genfake *fg;
	
	fg = calloc(1, sizeof(*fg));
	fg->gid.board = b;
	fg->acq = 0; /*acquisition is off by default*/
	fg->cf = calloc(1, sizeof(struct __adc_genfake_conf));
	fg->samplesize = 1;
	
	return (void *) &fg->gid;
}	

int adc_genfake_close(struct adc_dev *dev)
{
	
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	
	free(fg->cf);
	free(fg);
	return 0;
}

int adc_genfake_acq_start(struct adc_dev *dev,
			     unsigned int flags,
			     struct timeval *timeout)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	int err = 0;
	
	fg->acq = 1; /*acquisition is on, now*/
	adc_genfake_tstamp_buffer(NULL, NULL);
	
	err = adc_genfake_acq_poll(dev, flags, timeout);
	if (err != 0)
		return -1;
	return 0;
}

int adc_genfake_acq_poll(struct adc_dev *dev, unsigned int flags,
		    struct timeval *timeout)
{
	return 0;
}

int adc_genfake_acq_stop(struct adc_dev *dev, unsigned int flags)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	fg->acq = 0; /*acquisition is off, now*/
	
	return 0;
}

int adc_genfake_set_param(struct adc_dev *dev, char *name, char *sptr, int *iptr)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	unsigned int ind, type;
	if (name == NULL) {
		ind = fg->conf_index;
		type = fg->conf_type;
	} else
		sscanf(name, "%d/%d", &type, &ind);

	switch (type) {
	case ADC_CONF_TYPE_ACQ:
		
		switch (ind) {
		case ADC_CONF_ACQ_N_SHOTS:
			fg->cf->nshots = *iptr;
			break;
		case ADC_CONF_ACQ_POST_SAMP:
			fg->cf->postsamples = *iptr;
			break;
		case ADC_CONF_ACQ_PRE_SAMP:
			fg->cf->presamples = *iptr;
			break;
		default:
			errno = ADC_ENOCAP;
			return -1;
		}
		break;
	default:
		errno = ADC_ENOCFG;
		return -1;	
	}
	
	return 0;
}

int adc_genfake_get_param(struct adc_dev *dev, char *name, char *sptr, int *iptr)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	
	unsigned int ind, type;
	if (name == NULL) {
		ind = fg->conf_index;
		type = fg->conf_type;
	} else
		sscanf(name, "%d/%d", &type, &ind);
	
	switch (type) {
	case ADC_CONF_TYPE_ACQ:
		switch (ind) {
			case ADC_CONF_ACQ_N_SHOTS:
				(*iptr) = fg->cf->nshots;
				break;
			case ADC_CONF_ACQ_POST_SAMP:
				(*iptr) = fg->cf->postsamples;
				break;
			case ADC_CONF_ACQ_PRE_SAMP:
				(*iptr) = fg->cf->presamples;
				break;
			default:
				errno = ADC_ENOCAP;
				return -1;
		}
		break;
	default:
		errno = ADC_ENOCFG;
		return -1;	
	}
	
	return 0;
}

int adc_genfake_apply_config(struct adc_dev *dev, unsigned int flags, 
				struct adc_conf *conf)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	int i, err, val;
	for (i=0; i < __ADC_CONF_LEN; i++) {
		if (!(conf->mask & (1LL << i)))
			continue;
		fg->conf_index = i;
		fg->conf_type = conf->type;
		val = conf->value[i];
		err = adc_genfake_set_param(dev, NULL, NULL, &val);	
		
		if (err != 0)
			return -1;		
				
	}
	return 0;
}

int adc_genfake_retrieve_config(struct adc_dev *dev, struct adc_conf *conf)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	int i, err, val;
	for (i=0; i < __ADC_CONF_LEN; i++) {
		if (!(conf->mask & (1LL << i)))
			continue;
		fg->conf_index = i;
		fg->conf_type = conf->type;
		err = adc_genfake_get_param(dev, NULL, NULL, &val);
		conf->value[i] = val;
		
		if (err != 0)
			return -1;		
	}
	return 0;
}


struct adc_buffer *adc_genfake_request_buffer(struct adc_dev *dev,
						   int nsamples,
						   void *(*alloc_fn)(size_t),
						   unsigned int flags)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	struct adc_buffer *buf;
	
	buf = calloc(1, sizeof(*buf));
	if (!buf) {
		errno = ENOMEM;
		return NULL;
	}
	
	buf->metadata = NULL;
	
	if (!alloc_fn)
		alloc_fn = malloc;
	if (alloc_fn) {
		buf->data = alloc_fn(nsamples * fg->samplesize);
		if (!buf->data) {
			free(buf->metadata);
			free(buf);
			errno = ENOMEM;
			return NULL;
		}
	}
	/*THIRD OPTION: USE ONLY MMAP*/
	buf->samplesize = fg->samplesize;
	buf->nsamples = nsamples;
	buf->dev = (void *)&fg->gid;
	buf->flags = flags;
	return buf;	
}						   


int adc_genfake_fill_buffer(struct adc_dev *dev,
			      struct adc_buffer *buf,
			      unsigned int flags,
			      struct timeval *timeout)
{
	struct __adc_dev_genfake *fg = container_of(dev, struct __adc_dev_genfake, gid);
	struct timespec tp;
	
	/*Useful if you mean to add channels to the generic fake boards and want to disable only 
	some of them*/
	if (!fg->acq) {
		errno = ADC_EDISABLED;
		return -1;
	}
	
	int samplesize = buf->samplesize;
	int i, datalen = samplesize * buf->nsamples;
	uint8_t datum;
	clock_gettime(CLOCK_REALTIME, &tp);
	srand(tp.tv_nsec);
	
	for (i=0; i < datalen; i += sizeof(datum)) {
		datum = rand() % 256;
		sprintf(buf->data+i, "%c", datum);
	}
	
	return 0;
}

struct adc_timestamp *adc_genfake_tstamp_buffer(struct adc_buffer *buf,
							struct adc_timestamp *ts)
{
	struct timespec tp;
	
	static unsigned long start;
	if (ts == NULL) { /*Start timer*/
		clock_gettime(CLOCK_REALTIME, &tp);
		start = tp.tv_nsec;		
						
	}
	if (ts) {
		clock_gettime(CLOCK_REALTIME, &tp);	
		ts->secs = tp.tv_sec;
		ts->ticks = tp.tv_nsec;
		ts->bins = tp.tv_nsec - start; /*acquisition time in nsecs*/
		return ts;
	}
	
	return NULL;
}

int adc_genfake_release_buffer(struct adc_dev *dev,
				 struct adc_buffer *buf,
				 void (*free_fn)(void *))
{
	
	if (!free_fn)
		free_fn = free;

	if (free_fn)
		free_fn(buf->data);
	
	free_fn(buf);
	
	return 0;
}









