#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <poll.h>
#include <sys/stat.h>

#include <linux/zio-user.h>

#include "adc-lib.h"
#include "adc-lib-int.h"

#define FAKE_ZIO_DEV_PATH "/dev/zio"
#define FAKE_ZIO_SYS_PATH "/sys/bus/zio/devices"


struct adc_dev *adc_ziofake_open(const struct adc_board_type *b,
				   	unsigned int dev_id,
				   	unsigned long totalsamples,
				   	unsigned int nbuffer,
				   	unsigned long flags){
	
	struct adc_dev *dev = adc_zio_open(b, dev_id, totalsamples, nbuffer, flags);
	
	struct __adc_dev_zio *fk = to_dev_zio(dev);
	fk->samplesize = 1; /* FIXME: should read sysfs instead -- where? */
	dev = (void *) &fk->gid;
	
	return dev;
	}


