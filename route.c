/*
 * Routing public functions to device-specific code
 *
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2 as published by the Free Software Foundation or, at your
 * option, any later version.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "adc-lib.h"
#include "adc-lib-int.h"

int adc_acq_start(struct adc_dev *dev,
			     unsigned int flags,
			     struct timeval *timeout)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->acq_start(dev, flags, timeout);
}

int adc_acq_poll(struct adc_dev *dev, unsigned int flags,
		    struct timeval *timeout)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->acq_poll(dev, flags, timeout);
}

int adc_acq_stop(struct adc_dev *dev, unsigned int flags)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->acq_stop(dev, flags);
}

int adc_apply_config(struct adc_dev *dev, unsigned int flags,
			struct adc_conf *conf)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;
	uint64_t cap_mask;

	if (!conf->mask) {
		errno = ADC_ENOMASK;
		return -1; /* Nothing to do */
	}
	cap_mask = b->capabilities[conf->type];
	if ((cap_mask & conf->mask) != conf->mask) {
		/* Unsupported capabilities */
		errno = ADC_ENOCAP;
		return -1;
	}
	return b->adc_op->apply_config(dev, flags, conf);
}

int adc_retrieve_config(struct adc_dev *dev, struct adc_conf *conf)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;
	uint64_t cap_mask;

	if (!conf->mask) {
		errno = ADC_ENOMASK;
		return -1; /* Nothing to do */
	}
	cap_mask = b->capabilities[conf->type];
	if ((cap_mask & conf->mask) != conf->mask) {
		/* Unsupported capabilities */
		errno = ADC_ENOCAP;
		return -1;
	}
	return b->adc_op->retrieve_config(dev, conf);
}

int adc_get_param(struct adc_dev *dev, char *name,
		     char *sptr, int *iptr)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->get_param(dev, name, sptr, iptr);
}

int adc_set_param(struct adc_dev *dev, char *name,
		     char *sptr, int *iptr)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->set_param(dev, name, sptr, iptr);
}

struct adc_buffer *adc_request_buffer(struct adc_dev *dev,
					    int nsamples,
					    void *(*alloc)(size_t),
					    unsigned int flags)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->request_buffer(dev, nsamples, alloc, flags);
}

int adc_fill_buffer(struct adc_dev *dev,
		       struct adc_buffer *buf,
		       unsigned int flags,
		       struct timeval *timeout)
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->fill_buffer(dev, buf, flags, timeout);
}

struct adc_timestamp *adc_tstamp_buffer(struct adc_buffer *buf,
					      struct adc_timestamp *ts)
{
	struct adc_gid *g = (void *)buf->dev;
	const struct adc_board_type *b = g->board;

	return b->adc_op->tstamp_buffer(buf, ts);
}

int adc_release_buffer(struct adc_dev *dev, struct adc_buffer *buf,
			  void (*free)(void *))
{
	struct adc_gid *g = (void *)dev;
	const struct adc_board_type *b = g->board;

	if (!buf)
		return 0;

	return b->adc_op->release_buffer(dev, buf, free);
}
