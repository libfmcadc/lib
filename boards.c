/*
 * All the boards in the library
 *
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2 as published by the Free Software Foundation or, at your
 * option, any later version.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include "adc-lib.h"
#include "adc-lib-int.h"

#define ADC_ZIO_TRG_MASK (1LL << ADC_CONF_TRG_SOURCE) |      \
			    (1LL << ADC_CONF_TRG_SOURCE_CHAN) | \
			    (1LL << ADC_CONF_TRG_THRESHOLD) |   \
			    (1LL << ADC_CONF_TRG_POLARITY) |    \
			    (1LL << ADC_CONF_TRG_DELAY)
#define ADC_ZIO_ACQ_MASK (1LL << ADC_CONF_ACQ_N_SHOTS) |     \
			    (1LL << ADC_CONF_ACQ_POST_SAMP) |   \
			    (1LL << ADC_CONF_ACQ_PRE_SAMP) |    \
			    (1LL << ADC_CONF_ACQ_DECIMATION) |  \
			    (1LL << ADC_CONF_ACQ_FREQ_HZ) |     \
			    (1LL << ADC_CONF_ACQ_N_BITS)
#define ADC_ZIO_CHN_MASK (1LL << ADC_CONF_CHN_RANGE) |       \
			    (1LL << ADC_CONF_CHN_TERMINATION) | \
			    (1LL << ADC_CONF_CHN_OFFSET)
#define ADC_ZIO_BRD_MASK (1LL << ADC_CONF_BRD_STATE_MACHINE_STATUS) | \
			    (1LL << ADC_CONF_BRD_N_CHAN) | \
			    (1LL << ADC_CONF_UTC_TIMING_BASE_S) | \
			    (1LL << ADC_CONF_UTC_TIMING_BASE_T)

struct adc_operations fa_100ms_4ch_14bit_op = {
	.open =			adc_100m14b4cha_open,
	.close =		adc_zio_close,

	.acq_start =		adc_100m14b4cha_acq_start,
	.acq_poll =		adc_zio_acq_poll,
	.acq_stop =		adc_100m14b4cha_acq_stop,

	.apply_config =		adc_zio_apply_config,
	.retrieve_config =	adc_zio_retrieve_config,

	.get_param =		adc_zio_get_param,
	.set_param =		adc_zio_set_param,

	.request_buffer =	adc_zio_request_buffer,
	.fill_buffer =		adc_zio_fill_buffer,
	.tstamp_buffer =	adc_zio_tstamp_buffer,
	.release_buffer =	adc_zio_release_buffer,
};

struct adc_operations fa_zio_fake_op = {
	.open = 		adc_ziofake_open,
	.close =		adc_zio_close,
	
	.acq_start =		adc_zio_acq_start,
	.acq_poll =		adc_zio_acq_poll,
	.acq_stop =		adc_zio_acq_stop,
	
	.apply_config = 	adc_zio_apply_config,
	.retrieve_config =	adc_zio_retrieve_config,
	
	.get_param =		adc_zio_get_param,
	.set_param =		adc_zio_set_param,
	
	.request_buffer =	adc_zio_request_buffer,
	.fill_buffer =		adc_zio_fill_buffer,
	.tstamp_buffer =	adc_zio_tstamp_buffer,
	.release_buffer =	adc_zio_release_buffer,
};

struct adc_operations fa_generic_fake_op = {
	.open = 		adc_genfake_open,
	.close =		adc_genfake_close,
	
	.acq_start =		adc_genfake_acq_start,
	.acq_poll = 		adc_genfake_acq_poll,
	.acq_stop =		adc_genfake_acq_stop,
	
	.apply_config = 	adc_genfake_apply_config,
	.retrieve_config =	adc_genfake_retrieve_config,
	
	.set_param = 		adc_genfake_set_param,
	.get_param =		adc_genfake_get_param,
	
	.request_buffer =	adc_genfake_request_buffer,
	.fill_buffer =		adc_genfake_fill_buffer,
	.tstamp_buffer =	adc_genfake_tstamp_buffer,
	.release_buffer =	adc_genfake_release_buffer,
};

struct adc_board_type fmcadc_100ms_4ch_14bit = {
	.name = "fmc-adc-100m14b4cha",	/* for library open() */
	.devname = "adc-100m14b",	/* for device named in /dev/zio */
	.driver_type = "zio",
	.capabilities = {
		ADC_ZIO_TRG_MASK,
		ADC_ZIO_ACQ_MASK,
		ADC_ZIO_CHN_MASK,
		ADC_ZIO_BRD_MASK,
	},
	.adc_op = &fa_100ms_4ch_14bit_op,
};

struct adc_board_type adc_ziofake = {
	.name = "adc-ziofake",
	.devname = "zzero",
	.driver_type = "zio",
	.capabilities = {
		ADC_ZIO_TRG_MASK,
		ADC_ZIO_ACQ_MASK,
		ADC_ZIO_CHN_MASK,
		ADC_ZIO_BRD_MASK,
	},
	.adc_op = &fa_zio_fake_op,
};

struct adc_board_type adc_genericfake = {
	.name = "adc-genericfake",
	.devname = "fakedev",
	.driver_type = "fakebus",
	.capabilities = {
		ADC_ZIO_TRG_MASK,
		ADC_ZIO_ACQ_MASK,
		ADC_ZIO_CHN_MASK,
		ADC_ZIO_BRD_MASK,
	},
	.adc_op = &fa_generic_fake_op,
};

/*
 * The following array is the main entry point into the boards
 */
static const struct adc_board_type *adc_board_types[] = {
	&fmcadc_100ms_4ch_14bit,
	&adc_ziofake,
	&adc_genericfake,
	/* add new boards here */
};

static const struct adc_board_type *find_board(char *name)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(adc_board_types); i++)
		if (!strcmp(name, adc_board_types[i]->name))
			return adc_board_types[i];
	errno = ENODEV;
	return NULL;
}


/* Open should choose the buffer type (FIXME) */
struct adc_dev *adc_open(char *name, unsigned int dev_id,
				      unsigned long buffersize,
				      unsigned int nbuffer,
				      unsigned long flags)
{
	const struct adc_board_type *b;

	b = find_board(name);
	if (!b)
		return NULL;

	return b->adc_op->open(b, dev_id, buffersize, nbuffer, flags);
}

#define ADC_PATH_PATTERN "/dev/%s.%d"

/* Open by lun should lookup a database */
struct adc_dev *adc_open_by_lun(char *name, int lun,
				      unsigned long buffersize,
				      unsigned int nbuffer,
				      unsigned long flags)
{
	ssize_t ret;
	char dev_id_str[8];
	char path[PATH_MAX];
	int dev_id;

	ret = snprintf(path, sizeof(path), "/dev/%s.%d",
		       "adc-100m14b" /* FIXME: this must be generic */,
		       lun);
	if (ret < 0 || ret >= sizeof(path)) {
		errno = EINVAL;
		return NULL;
	}
	ret = readlink(path, dev_id_str, sizeof(dev_id_str));
	if (sscanf(dev_id_str, "%4x", &dev_id) != 1) {
		errno = ENODEV;
		return NULL;
	}
	return adc_open(name, dev_id, buffersize, nbuffer, flags);

}

int adc_close(struct adc_dev *dev)
{
	struct adc_gid *b = (void *)dev;
	
	return b->board->adc_op->close(dev);
}
